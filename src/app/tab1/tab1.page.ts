import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { GoogleMaps, GoogleMap, Environment, Marker } from '@ionic-native/google-maps/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  map: GoogleMap;
  lat: -6.2413142;
  lng: 106.7801552;

  constructor(
    private platform: Platform,
    private geolocation: Geolocation
  ) {}

  async ngOnInit() {
    await this.platform.ready();

    await this.loadMap();
  }

  loadMap() {
    Environment.setEnv({
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyAMCU9vqP4TSoEXkCDmjcgaI_CXVF9Xg7U',
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyAMCU9vqP4TSoEXkCDmjcgaI_CXVF9Xg7U'
    });

    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp);
     }).catch((error) => {
       console.log('Error getting location', error);
     });

    const watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
        console.log(data.coords);
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
        this.map = GoogleMaps.create('map_canvas', {
        camera: {
          target: {
            lat: data.coords.latitude,
            lng: data.coords.longitude,
          },
          tilt: 30,
          zoom: 17
        }
      });
        this.map.addMarker({
        title: 'My Map',
        icon: 'blue',
        animation: 'DROP',
        position: {
          lat: data.coords.latitude,
          lng: data.coords.longitude
        }
      }).then((marker: Marker) => {
        marker.showInfoWindow();
      });
      });


  }

}
